<?php
/**
 * Created by PhpStorm.
 * User: Michel
 * Date: 8/08/16
 * Time: 11:58
 */


/*
 * stappen
- tabbladen omvormen tot nodige csv's
- redirects.dev (localhost) voor redirect_module laten lopen op alle files
- nginx/server.rewrites (/data/web/nginx/server.rewrites) op de server aanpassen (opletten of je na saven geen foutmelding krijgt
    Your Nginx configuration contains errors, please check
    /data/web/nginx/nginx_error_output to see them.
)
 */

function loadFiles($nameFile, $ngix = true)
{
    $newlist = 0;
    $oldlist = 0;
    $urlsArray = array();
    $csvFile = file($nameFile);
    $data = [];

    foreach ($csvFile as $line) {
        $data[] = str_getcsv($line, ";");
        $oldlist++;
    }

    foreach ($data as $line) {

        if ($line[0] && $line[1]) {
            if ($line[0] != $line[1]) {
                $url = $line[0];
                $queryString = false;
                if (strpos($url, '?')) {
                    $url = substr($url, 0, strpos($url, "?"));
                    //wegens te veel problemen met query strings, voorlopig geen rekening meer houden met query strings
                    $queryString = false;
                }
                //opnieuw controleren of twee lijnen verschillend zijn, na het afkappen van de query string
                if ($url != $line[1]) {
                    if (strpos($url, '{') != true) {
                        if (strpos($url, '|') != true) {
                            if (strpos($url, '(') != true) {
                                if (strpos($url, 'admin') != true) {
                                    //rewrite ^/banden/17-inch/225/$ $scheme://$http_host/nl/banden-en-velgen/banden.html permanent;
                                    //rewrite ^/fr-BE/idees/idees-pour-chaque-piece/chambre-d-enfant$ $scheme://$http_host/fr/images-inspirantes permanent;
                                    //Redirect 301 /en/Contact/       http://www.lamett.eu/contact?___store=english

                                    if (!in_array($url, $urlsArray)) {
                                        $urlsArray[] = $url;
                                        $newlist++;
                                        if ($ngix) {
                                            echo "rewrite ^";
                                            echo htmlspecialchars(str_replace(' ', '%20', $url));
                                            if ($queryString) {
                                                echo " &#36;scheme://&#36;http_host";
                                            } else {
                                                echo "&#36; &#36;scheme://&#36;http_host";
                                            }
                                            echo htmlspecialchars(str_replace(' ', '%20', $line[1]));
                                            echo " permanent;";

                                        } else {
                                            echo " Redirect 301 ";
                                            echo htmlspecialchars(str_replace(' ', '%20', $url));
                                            echo " http://www.bpluspharma.be";
                                            echo htmlspecialchars(str_replace(' ', '%20', $line[1]));
                                        }
                                        echo "<br/>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    //echo "orgi: ".$oldlist ."<br/>";
    // echo "new: ".$newlist ."<br/>";
}


loadFiles("nieuwe redirects.csv");

//loadFiles("example.csv",false);
